// Ionic BBJExport App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'BBJExport' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'BBJExport.controllers' is found in controllers.js
//  SERVERURL contains the server url to get service which use globally

//var BASE_URL="http://bbjexport.jm-technologies.in/image/"; //For online
  var BASE_URL="http://192.168.1.82/BBJExport/images/"; //For Testing
  var BASE_URLs="http://192.168.1.82/BBJExport/"
//var BASE_URL="http://192.168.1.157/BBJExport/";   //For Locahost
  var phpservice = "customerinfo.php";
  var link = BASE_URLs+ phpservice;


/*
* partial payment percentage for normal and wholesaler user
*/

var normal_partial_payment = 75;
var wholesaler_partial_payment = 50;

angular.module('BBJExport', ['ionic','BBJExport.controllers',
                             'BBJExport.services','BBJExport.directives',
                             'BBJExport.filters','ngCordova',
                             'ngSanitize'
                            ])

.run(function($ionicPlatform, ConnectivityMonitor,$state,$ionicHistory,$ionicPopup) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

   // Disable BACK button on home
      $ionicPlatform.registerBackButtonAction(function (event) {
	  if($state.current.name=="app.products"){
			 $ionicPopup.confirm({
				title: 'BBJ Export',
				content: 'Do you want to close ?'
			}).then(function(res) {
			if(res) {
				navigator.app.exitApp();
			}
		});
	  }else if($state.current.name=="app.product" || $state.current.name=="app.orderinfo" || $state.current.name=="app.orderreturndetail" 
                    || $state.current.name=="app.billcheckout" || $state.current.name=="app.deliverycheckout" 
                    || $state.current.name=="app.deliverymethodcheckout" || $state.current.name=="app.paymentmethodcheckout" 
                    || $state.current.name=="app.showorderproduct" || $state.current.name=="app.customizeOrderList"
                    || $state.current.name=="app.orderreturns" || $state.current.name=="app.orderdetails"
                    || $state.current.name=="app.orderinfo" || $state.current.name=="app.orderreturndetail") {

		     navigator.app.backHistory();
	  }else{
                      $ionicHistory.nextViewOptions({
                               disableBack: true
                     });
		  $state.go('app.products');
	  }
	}, 100);
     //Check Network is available or not
     ConnectivityMonitor.startWatching();
  });
})


.config(function($stateProvider,$httpProvider, $urlRouterProvider) {
  
  $httpProvider.defaults.timeout = 5000;

  $stateProvider
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/login/register.html',
        controller: 'RegisterCtrl'
      }
    }
  })
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login/login.html',
        controller: 'LoginCtrl'
      }
    }
   })
   .state('app.categoryproducts', {
    url: '/categoryproducts/:categoryId/:categoryTitle',
    views: {
      'menuContent': {
        templateUrl: 'templates/category-products.html',
        controller: 'CategoryProductsCtrl'
      }
    }
  })
    .state('app.products', {
      url: '/products',
      views: {
        'menuContent': {
          templateUrl: 'templates/products.html',
          controller: 'ProductsCtrl'
        }
      }
    })

  .state('app.product', {
    url: '/products/:productId/:productPrice/:productTitle',
    views: {
      'menuContent': {
        templateUrl: 'templates/product-details.html',
        controller: 'ProductDetailsCtrl'
      }
    }
  })

  .state('app.orderdetails', {
      url: '/orderdetails',
      views: {
        'menuContent': {
          templateUrl: 'templates/order/order-details.html',
          controller: 'OrderDetailsCtrl'
        }
      }
    })
  .state('app.orderinfo', {
      url: '/orderdetails/:productId',
      views: {
        'menuContent': {
          templateUrl: 'templates/order/order-info.html',
          controller: 'OrderInfoCtrl'
        }
      }
    })
  .state('app.orderreturns', {
      url: '/orderreturns',
      views: {
        'menuContent': {
          templateUrl: 'templates/order/order-return-list.html',
          controller: 'OrderReturnsCtrl'
        }
      }
    })
  
 .state('app.orderreturndetail', {
      url: '/orderreturns/:returnId',
      views: {
        'menuContent': {
          templateUrl: 'templates/order/order-return-details.html',
          controller: 'OrderReturnsDetailsCtrl'
        }
      }
    })
  .state('app.contactus', {
      url: '/contactus',
      views: {
        'menuContent': {
          templateUrl: 'templates/contact-us.html',
          controller: 'ContactUsCtrl'
        }
      }
    })
  .state('app.usercart', {
      url: '/usercart',
      views: {
        'menuContent': {
          templateUrl: 'templates/usercart.html',
          controller: 'UserCartCtrl'
        }
      }
    })
   .state('app.billcheckout', {
      url: '/billcheckout',
      views: {
        'menuContent': {
          templateUrl: 'templates/checkout/checkout-billing.html',
          controller: 'BillCheckoutCtrl'
        }
      }
    })
   .state('app.deliverycheckout', {
      url: '/deliverycheckout/:isExist/:firstname/:lastname/:address_1/:address_2/:city/:postcode/:country_name/:zone_name',
      views: {
        'menuContent': {
          templateUrl: 'templates/checkout/checkout-delivery.html',
          controller: 'DeliveryCheckoutCtrl'
        }
      }
    })
    .state('app.deliverymethodcheckout', {
      url: '/deliverymethodcheckout/:checkoutTax',
      views: {
        'menuContent': {
          templateUrl: 'templates/checkout/delivery-method.html',
          controller: 'DeliveryMethodCheckoutCtrl'
        }
      }
    })
    .state('app.showorderproduct', {
      url: '/showorderproduct/:checkoutTax/:partialPayment',
      views: {
        'menuContent': {
          templateUrl: 'templates/checkout/show-order-product.html',
          controller: 'ShowOrderProductCtrl'
        }
      }
    })
   .state('app.paymentmethodcheckout', {
      url: '/paymentmethodcheckout/:checkoutTax',
      views: {
        'menuContent': {
          templateUrl: 'templates/checkout/payment-method.html',
          controller: 'PaymentMethodCheckoutCtrl'
        }
      }
    })
  .state('app.wishlist', {
      url: '/wishlist',
      views: {
        'menuContent': {
          templateUrl: 'templates/wishlist.html',
          controller: 'WishlistCtrl'
        }
      }
    })
  .state('app.useraccount', {
      url: '/useraccount',
      views: {
        'menuContent': {
          templateUrl: 'templates/user-account.html',
          controller: 'UserAccountCtrl'
        }
      }
    })
 .state('app.wholesaler', {
      url: '/wholesaler',
      views: {
        'menuContent': {
          templateUrl: 'templates/wholesaler/wholesaler.html',
          controller: 'WholesalerCtrl'
        }
      }
    })
 .state('app.help', {
      url: '/help',
      views: {
        'menuContent': {
          templateUrl: 'templates/help.html',
          controller: 'HelpCtrl'
        }
      }
    })

 .state('app.customizeOrder', {
      url: '/customizeOrder',
      views: {
        'menuContent': {
          templateUrl: 'templates/customizeOrder/customize-order.html',
	  controller: 'CustomizeOrderCtrl'	
         
        }
      }
    })

.state('app.customizeOrderList', {
      url: '/customizeOrderList',
      views: {
        'menuContent': {
          templateUrl: 'templates/customizeOrder/customize-order-list.html',
	  controller: 'CustomizeOrderListCtrl'	
         
        }
      }
    })

  .state('app.aboutus', {
      url: '/aboutus',
      views: {
        'menuContent': {
          templateUrl: 'templates/about-us.html',
          controller: 'AboutUsCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback 			
	$urlRouterProvider.otherwise('/app/products');  
});
