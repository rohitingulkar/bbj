angular.module('BBJExport.services', [])
 
//Factory to connectivity Monitor
.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork,$ionicPopup){
 
  return {

    //To check network is online or online
    isOnline: function(){
      if(ionic.Platform.isWebView()){
        return $cordovaNetwork.isOnline();   
      } else {
        return navigator.onLine;
      }
    }, 
    
    //To check network is online or offline
    isOffline: function(){
      if(ionic.Platform.isWebView()){
        return !$cordovaNetwork.isOnline();   
      } else {
        return !navigator.onLine;
      }
    },

    //To watching network events
    startWatching: function(){
        if(ionic.Platform.isWebView()){
 
          $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            //alert("Network Availbale.");
          });
 
          $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            var alertPopup = $ionicPopup.alert({
	     title: 'Internet Disconnected',
	     template: 'The internet is disconnected on your device.'
	   });
	   alertPopup.then(function(res) {
	     //console.log('The internet is disconnected on your device.');
	   });
         });
 
        }
        else {
 
          window.addEventListener("online", function(e) {
            //alert("Network Availbale.");
          }, false);   
 
          window.addEventListener("offline", function(e) {
            var alertPopup = $ionicPopup.alert({
	     title: 'Internet Disconnected',
	     template: 'The internet is disconnected on your device.'
	   });
	   alertPopup.then(function(res) {
	    // console.log('The internet is disconnected on your device.');
	   });
          }, false); 
        }      
    }
  }
})

//Factory to Utility
.factory('Utility', function($http,$ionicPopup,$cordovaToast) {
  var self = this;

  self.showAlert = function(title_msg,template_msg) {
	   var alertPopup = $ionicPopup.alert({
	     title: title_msg,
	     template: template_msg
	   });
	   alertPopup.then(function(res) {
	     //console.log('Thank you.');
	   });
   };
  self.showToast = function(msg) {
      if(ionic.Platform.isWebView()){
           var duration = 'short' ; //  Options : 'short', 'long'
           var position = 'center'; // Options : 'top', 'center', 'bottom'
	   $cordovaToast
             .show(msg, duration, position)
             .then(function(success) {
                   console.log("The toast was shown");
              }, function (error) {
                   console.log("The toast was not shown due to " + error);
           });
      }else{
          console.log("Toast Not support !!");
      }
   };
  return self;
})

//Factory to user Info
.factory('ProductInfo', function($http) {
       var self = this;
       self.productInformation = function (type,id) {
                  var params=null;

                  if(angular.equals(type,"product_categories")){
                       params = { 
                                    request_type: type
                                  };
                  }else if(angular.equals(type,"home_screen")){
                       params = { 
                                    request_type: type
                                  };
                  }else if(angular.equals(type,"product_by_categories_id")){
                      params = { 
                                    request_type: type,catagory_id:id
                                };
                  }else if(angular.equals(type,"product_by_product_id")){
                      params = { 
                                    request_type: type,product_id:id
                                };
                  }
                
		  return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                  //console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       return self;
})

//Factory to Address Info
.factory('AddressInfo', function($http) {
       var self = this;
       self.addressInformation = function (type,id) {
                  var params=null;

                  if(angular.equals(type,"country")){
                       params = { 
                                    request_type: type
                                  };
                  }else if(angular.equals(type,"state")){
                      params = { 
                                    request_type: type,country_id:id
                                };
                  }
                
		  return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                  //console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       return self;
})

//Factory to user Info
.factory('UserInfo', function($http) {

	var self = this;

     //User Information
       
      self.userInformation = function (user,type,cust_id,imei_number) {
                  var params=null;

                  if(angular.equals(type,"customer_account_info_get")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                  };
                  }else if(angular.equals(type,"customer_account_info_update")){
                      params = { 
                                    request_type: type,firstname:user.firstname,lastname: user.lastname,email : user.email,
                                    telephone: user.telephone,fax: user.fax,customer_id:user.customer_id,imei_no: imei_number
                                };
                  }else if(angular.equals(type,"customer_account_password_update")){
                       params = { 
                                    request_type: type,customer_id:user.customer_id,password:user.newpassword,imei_no:imei_number
                                  };
                  }else if(angular.equals(type,"customer_account_address_get")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                  };
                  }else if(angular.equals(type,"customer_account_address_update")){
                       params = { 
                                    request_type: type,customer_id:user.customer_id,firstname:user.firstname,lastname:user.lastname,
                                    company:user.company,address_1:user.address_1,address_2:user.address_2,city:user.city,
                                    postcode:user.postcode,country_name:user.country_name,zone_name:user.zone_name,imei_no:imei_number
                                  };
                  }else if(angular.equals(type,"forgot_password")){
                       params = { 
                                    request_type: type,email:user.forgotemail,imei_no:imei_number
                                  };
                  }else if(angular.equals(type,"register_user")){
                      
                      
                      params = { 
                                    request_type: type,firstname: user.firstname,lastname: user.lastname,email : user.email,
                                    telephone: user.telephone,fax: user.fax,company: user.company,address_1: user.address_1,
                                    address_2: user.address_2,country_id: user.country,zone_id: user.state,city: user.city,
                                    postcode: user.postcode,password: user.password,imei_no: imei_number,
                                    customertype:user.customertype
                                  };
                                  
                  }else if(angular.equals(type,"login")){
                                params = { 
                                    request_type: type,email: user.email,password:user.password,imei_no:imei_number
                                  };
                  }

       		  
		  return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                  //console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       
       return self;
})

//Factory to user wish list
.factory('WishList', function($http) {
     var self = this;

      self.userWishlist = function (type,cust_id,prod_id) {
        var params=null;

        if(angular.equals(type,"wishlist_add_product")){
                       params = { 
                                    request_type: type,customer_id:cust_id,product_id:prod_id
                                };
        }else if(angular.equals(type,"wishlist_show_all")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                };
        }else if(angular.equals(type,"wishlist_product_remove")){
                       params = { 
                                    request_type: type,customer_id:cust_id,product_id:prod_id
                                };
        }
        
        
         return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
         }).then(function(response) {
               //console.log(JSON.stringify(response.data));
	       //alert(JSON.stringify(response.data));
	       return response.data;
	});
      }

     return self;
})

//Factory to user cart 
.factory('UserCart', function($http) {
     var self = this;

      self.userCart = function (type,cust_id,prod_id,prod_quantity) {
        var params=null;

        if(angular.equals(type,"add_to_cart")){
                       params = { 
                                    request_type: type,customer_id:cust_id,product_id:prod_id,quantity:prod_quantity
                                };
        }else if(angular.equals(type,"cart_show_all")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                };
        }else if(angular.equals(type,"cart_remove")){
                       params = { 
                                    request_type: type,customer_id:cust_id,product_id:prod_id
                                };
        }else  if(angular.equals(type,"cart_update")){
                       params = { 
                                    request_type: type,customer_id:cust_id,product_id:prod_id,quantity:prod_quantity
                                };
        }else  if(angular.equals(type,"customer_account_address_get_show")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                };
        }else if(angular.equals(type,"count_cart_wishlist")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                };
        }  
        
         return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
         }).then(function(response) {
               //console.log(JSON.stringify(response.data));
	       //alert(JSON.stringify(response.data));
	       return response.data;
	});
      }

     return self;
})

//Factory to checkout
.factory('CheckOut', function($http) {
  var self = this;
  self.userCheckOut = function (type,cust_id,ime_no,user) {
    var params=null;
    if(angular.equals(type,"order_add_bill_exist_delivery_exist")){
                       params = { 
                                    request_type: type,customer_id:cust_id,imei_no:ime_no
                                };
    }else if(angular.equals(type,"order_add_bill_new_delivery_exist")){
                       params = { 
                                    request_type: type,customer_id:cust_id,imei_no:ime_no,bill_firstname:user.bill_firstname,
                                    bill_lastname:user.bill_lastname,bill_address_1:user.bill_address_1,
                                    bill_address_2:user.bill_address_2,bill_city:user.bill_city,
                                    bill_postcode:user.bill_postcode,bill_country_name:user.bill_country_name,
                                    bill_zone_name:user.bill_zone_name
                                };
    }else if(angular.equals(type,"order_add_bill_exist_delivery_new")){
                       params = { 
                                    request_type: type,customer_id:cust_id,imei_no:ime_no,del_firstname:user.del_firstname,
                                    del_lastname:user.del_lastname,del_address_1:user.del_address_1,
                                    del_address_2:user.del_address_2,del_city:user.del_city,
                                    del_postcode:user.del_postcode,del_country_name:user.del_country_name,
                                    del_zone_name:user.del_zone_name
                                };
    }else if(angular.equals(type,"order_add_bill_new_delivery_new")){
                       params = { 
                                    request_type: type,customer_id:cust_id,imei_no:ime_no,bill_firstname:user.bill_firstname,
                                    bill_lastname:user.bill_lastname,bill_address_1:user.bill_address_1,
                                    bill_address_2:user.bill_address_2,bill_city:user.bill_city,
                                    bill_postcode:user.bill_postcode,bill_country_name:user.bill_country_name,
                                    bill_zone_name:user.bill_zone_name,del_firstname:user.del_firstname,
                                    del_lastname:user.del_lastname,del_address_1:user.del_address_1,
                                    del_address_2:user.del_address_2,del_city:user.del_city,
                                    del_postcode:user.del_postcode,del_country_name:user.del_country_name,
                                    del_zone_name:user.del_zone_name
                                };
    }else if(angular.equals(type,"checkout_add")){
                       params = { 
                                    request_type: type,customer_id:cust_id,
                                    subtotal:user.subtotal,shippingrate:user.shippingrate,vat:user.vat,grandtotal:user.grandtotal
                                };
    }
  
     return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
      }).then(function(response) {
               //console.log(JSON.stringify(response.data));
	       //alert(JSON.stringify(response.data));
	       return response.data; 
      });
  }
 return self;
})

//Factory to order Info
.factory('UserOrderInfo', function($http) {
       var self = this;
       self.orderInformation = function (type,cust_id,orderid) {
                  var params=null;

                  if(angular.equals(type,"order_history")){
                       params = { 
                                    request_type: type,customer_id:cust_id
                                  };
                  }else if(angular.equals(type,"order_details_information")){
                      params = { 
                                    request_type: type,customer_id:cust_id,order_id:orderid
                                };
                  }
              
		  return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                  //console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       return self;
})

//Factory to user order return
.factory('UserOrderReturn', function($http) {
       var self = this;
       self.orderReturnProduct = function (type,user,cust_id,id) {
                  var params=null;
                  if(angular.equals(type,"return_product_show_edit")){
                      params = { 
                                    request_type: type,oc_order_product_id:id
                                };
                  }else if(angular.equals(type,"return_product_add")){
                      params = { 
                                    request_type: type,customer_id:cust_id,firstname: user.firstname,lastname: user.lastname,
                                    email : user.email,telephone:user.telephone,order_id:user.order_id,
                                    product_name:user.name,model_name:user.model,
                                    reason_for_return:user.reason_for_return,product_open:user.product_open,
                                    product_id:user.product_id,dateadded:user.date_added
                                };
                  }else if(angular.equals(type,"return_show_product")){
                            params = { 
                                      request_type: type,customer_id:cust_id
                                     };
                  }else if(angular.equals(type,"return_product_information")){
                          params = { 
                                     request_type: type,return_id:id
                          };
                  }
                
		 return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                  //console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       return self;
})

//Factory to Whollseller
.factory('Wholesaler', function($http) {
       var self = this;
       self.getWhollseller = function (type) {
                  var params=null;

                  if(angular.equals(type,"wholesaler")){
                       params = { 
                                    request_type: type
                                  };
                  }
                 
		  return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                  //console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       return self;
})

//Factory to Customize Order
.factory('CustomizeOrder', function($http) {
       var self = this;
       self.userCustomizeOrder = function (type,customerid,user,path) {
                  var params=null;

                  if(angular.equals(type,"customized_order")){
                       params = { 
                                    request_type: type,customer_id:customerid,productname:user.productname,productsize:user.productsize,
                                    productquantity:user.productquantity,productdescription:user.productdecription,comment:user.comment,
                                    date_expected:user.expected_date,image_title:user.image_title,image_path:path
                                };
                  }else if(angular.equals(type,"customized_order_list")){
                       params = { 
                                    request_type: type,customer_id:customerid
                                };
                  }
            
		  return $http({
			headers: {'Accept': "application/json",'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'},
			url: link,
			method: "POST",
			data:params,
		 }).then(function(response) {
                   console.log(JSON.stringify(response.data));
		  //alert(JSON.stringify(response.data));
		  return response.data;
		 });
       }
       return self;
});
