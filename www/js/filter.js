angular.module('BBJExport.filters', [])
.filter('htmlToPlaintext', function() {
    return function(text) {
      return text.replace(/\/>/g,'')
         .replace(/<\//g,'')
         .replace(/[<>]/g,'');
    }
  });
