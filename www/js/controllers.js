angular.module('BBJExport.controllers', [])

.controller('AppCtrl', function($scope,$rootScope,$ionicModal,$ionicPopup,
                                $timeout,$ionicPopover,$state,$ionicLoading,ProductInfo,
                                $ionicHistory,$window,UserCart,Utility) {
  
   $ionicPopover.fromTemplateUrl('templates/popover.html', {
    scope: $scope,
   }).then(function(popover) {
    $scope.popover = popover;
   });  

   //Go to User Cart
   $scope.userCart = function () {
    /*$ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
                               disableBack: true
    });*/
      $state.go('app.usercart');
   };

   if (window.localStorage.getItem("isLoggedIn") == undefined || window.localStorage.getItem("isLoggedIn") == 'false'){ 
      $rootScope.loginstatus = false;
      $rootScope.userwholesaler = false;
   }else{
      $rootScope.loginstatus = true;
      var customer_id = window.localStorage.getItem("customer_id");
      var customer_group_id = window.localStorage.getItem("customer_group_id");
       
      //To Identify user is Wholesaler or not  by customer_group_id == 2
      if(angular.equals(customer_group_id,"2")){
       $rootScope.userwholesaler = true;
      }else{
       $rootScope.userwholesaler = false;
      }
       
       UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
          $rootScope.cartcount = response.cart;
      }, function(err) {
     });
   }

  //Function to logout user
  $scope.logOut = function() {
   var confirmPopup = $ionicPopup.confirm({
     title: 'Confirm Logout',
     template: 'Are you sure you want to Logout?'
   });

   confirmPopup.then(function(res) {
     if(res) {
       $window.localStorage.clear();   
       $rootScope.userwholesaler = false;
       $rootScope.loginstatus = false;
       $ionicHistory.clearHistory()
       $ionicHistory.nextViewOptions({
                               disableBack: true,
                               historyRoot: true
        });
       Utility.showAlert("Success","Logout Successfully"); 
       $state.go('app.products');
     }
   });
 };

 //Function to go login state
 $scope.logIn = function() {
    $window.localStorage.clear();
    $rootScope.loginstatus = false;
    $ionicHistory.nextViewOptions({
                               disableBack: true
    });
    $state.go('app.login');
 };

 //service to get categories
 ProductInfo.productInformation("product_categories",0).then(function(response) {
          $scope.categories = response;
      }, function(err) {
   });
  
  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };

})

//Controller for User Login  
.controller('LoginCtrl', function($scope,$state,$rootScope,$stateParams,$ionicLoading,
                                  $ionicModal, $ionicHistory,UserInfo,UserCart,Utility) {

  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");
  var imei_number = "";

   $scope.user = {
    email: "",
    password: ""
  }
  //Login Function
  $scope.doLogin = function(loginData) {
    var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
    });

     UserInfo.userInformation(loginData,"login","","imei_number").then(function(response) {
         $ionicLoading.hide();
         if(angular.equals(response.code,"error")){
            window.localStorage.setItem("isLoggedIn", false);
            $rootScope.loginstatus = false;
            Utility.showAlert("Error","Enter Valid Credintials.");
         }else{
          UserCart.userCart("count_cart_wishlist",response.code,0,0).then(function(response) {
              $rootScope.cartcount = response.cart;
            }, function(err) {
          });
          window.localStorage.setItem("isLoggedIn", true);
          window.localStorage.setItem("useremail", loginData.email);
          window.localStorage.setItem("customer_id", response.code);
          window.localStorage.setItem("customer_group_id", response.customer_group_id);
          
          //To Identify user is Wholesaler or not  by customer_group_id == 2
          if(angular.equals(response.customer_group_id,"2")){
              $rootScope.userwholesaler = true;
          }else{
              $rootScope.userwholesaler = false;
          }


          $scope.user.email = "";
          $scope.user.password = "";
          $rootScope.loginstatus = true; 
          $ionicHistory.clearHistory()
          $ionicHistory.nextViewOptions({
                               disableBack: true,
                               historyRoot: true
          });
          $state.go('app.products');
         }
         
      }, function(err) {
         window.localStorage.setItem("isLoggedIn", false);
         $ionicLoading.hide();
         $rootScope.loginstatus = false;
         Utility.showAlert("Error","Failed to Login !");
     });
  };

   //Method to show register state
   $scope.showRegister = function() {
     /* $ionicHistory.clearHistory()
      $ionicHistory.nextViewOptions({
               disableBack: true
      });*/
     $state.go('app.register');
   }
    ;
   //Method to show forgot password modal
    $scope.showForgotPsw = function() {
       $scope.showModal('templates/login/forgot-password.html');
    };
     
    //Reset the password
     $scope.reset = function(user) {
               var loading = $ionicLoading.show({
                                               content: 'Sending',
                                               animation: 'fade-in',
                                               showBackdrop: true,
                                               maxWidth: 200,
                                               showDelay: 0
                });
	       UserInfo.userInformation(user,"forgot_password",customer_id,imei_number).then(function(response) {
		      $ionicLoading.hide();
		      if(angular.equals(response.code,"error")){
                         Utility.showAlert("Error","Email address does not exist");
		      }else if(angular.equals(response.code,"success")){
                          $ionicHistory.clearHistory()
                          $ionicHistory.nextViewOptions({
                               disableBack: true,
                               historyRoot: true
                          });
                          $state.go('app.products');
                          Utility.showAlert("Success","An email has been sent to you with instructions on resetting your password.");
                       $scope.closeModal();
		      }else{
                        Utility.showAlert("Error","An unknown error has occurred, please try again");
                      }
		   }, function(err) {
                      Utility.showAlert("Error","An unknown error has occurred, please try again");
		      $ionicLoading.hide();
		 });

    };

    //Method to open templates in modal
    $scope.showModal = function(templateUrl) {
	  $ionicModal.fromTemplateUrl(templateUrl, {
	    scope: $scope
	  }).then(function(modal) {
	    $scope.modal = modal;
	    $scope.modal.show();
	  });
    };

  //Method to close modal
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
})

  //Controller for product list
.controller('ProductsCtrl', function($ionicPlatform,$timeout,$scope,$cordovaNetwork,$ionicScrollDelegate,
                                     $ionicSlideBoxDelegate,$ionicLoading,ProductInfo) {

  $scope.BASE_URL = BASE_URL;   

  $ionicPlatform.ready(function() {
  $scope.network = true;
   //To check network is online or offline
   if(ionic.Platform.isWebView())  $scope.network = $cordovaNetwork.isOnline();
   var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
   });
   ProductInfo.productInformation("home_screen",0).then(function(response) {
        $ionicLoading.hide();
        $scope.banners = response['banner'];
        $scope.products = response['product'];
    }, function(err) {
          $ionicLoading.hide();
   });
    $scope.repeatDone = function() {
	    $ionicSlideBoxDelegate.update();
    };
  });
}) 
 
 //Controller for to User Register 
.controller('RegisterCtrl', function($scope, $stateParams,AddressInfo,$ionicLoading,$state,
                                     $ionicHistory,$ionicSlideBoxDelegate,UserInfo,Utility) {

  var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
  });



  //service to get countries on register controller
  AddressInfo.addressInformation("country",0).then(function(response) {
          $scope.countries = response;
          $ionicLoading.hide();
      }, function(err) {
          $ionicLoading.hide();
          Utility.showAlert("Error","Failed to get Countries names !");
   });

 // Merthod to Register User
  $scope.registerUser = function(user) {
    //alert(JSON.stringify(user));
    loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
    });

    if(user.customertype)
    {
    
     //Method to User Registration
     UserInfo.userInformation(user,"register_user","","").then(function(response) {
          //$scope.users = response;
          console.log("response: " + response.status );
          $ionicLoading.hide();
          if(angular.equals(response.status,"true")){
            $ionicHistory.clearHistory()
            $ionicHistory.nextViewOptions({
               disableBack: true
            });
            $state.go('app.login');
            Utility.showAlert("User Registartion","You have registered successfully.");
          }else if(angular.equals(response.status,"exist")){
            Utility.showAlert("User Registartion","Email Allready Exist.");
          }else{
               Utility.showAlert("Error","Failed to Register User !");
          }
      }, function(err) {
         $ionicLoading.hide();
         Utility.showAlert("Error","Failed to Register User !!!");
     });

    }
    else
    {
            $ionicLoading.hide();
            alert("Please select Customer Group");
    }
     
     
   };

   //To get the states name by country Id
   $scope.getStateByCountryId = function(country) {
    AddressInfo.addressInformation("state",country.country_id).then(function(response) {
          $scope.states = response;
      }, function(err) {
    });
   }
})

//Controller to category wise products
.controller('CategoryProductsCtrl', function($scope, $stateParams,$window,ProductInfo) {

   $scope.categoryId = $stateParams.categoryId;
   $scope.categoryTitle = $stateParams.categoryTitle;
   $scope.BASE_URL = BASE_URL;
    
     /*
     * Calculate Screen Dimension 
     */
    $scope.calculateDimensions = function(gesture) {
    $scope.dev_width = $window.innerWidth;
    $scope.grid_height = $window.innerWidth*0.64;
    $scope.dev_height = $window.innerHeight;
   };
     
  angular.element($window).bind('resize', function(){
    $scope.$apply(function() {
      $scope.calculateDimensions();    
    });       
  });
     
  $scope.calculateDimensions();   

   ProductInfo.productInformation("product_by_categories_id",$scope.categoryId).then(function(response) {
          $scope.products = response.category_product_details;
          $scope.catimages = response.category_image_details;
      }, function(err) {
    });
})

 //Controller for Product Details 
.controller('ProductDetailsCtrl', function($scope, $stateParams,$ionicScrollDelegate,$ionicModal,
            $ionicLoading,$rootScope,ProductInfo,WishList,UserCart,Utility) {

   $scope.BASE_URL = BASE_URL;
   $scope.id = $stateParams.productId;
   $scope.title = $stateParams.productTitle;
   $scope.price = $stateParams.productPrice;
   $scope.quantity = { value: 1 };
   
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");

   //Get product from web service 
   ProductInfo.productInformation("product_by_product_id",$scope.id).then(function(response){
	      $scope.product = response;
              //alert(JSON.stringify(response));
   });

  $scope.zoomMin = 1;
  

  //Add to wish list
  $scope.addToWishList = function() {
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           WishList.userWishlist("wishlist_add_product",customer_id,$scope.id).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Add to Wish List,Try after sometime.");
              }else if(angular.equals(response.code,"exist")){
                 Utility.showAlert("Success","Allready Exist in Wishlist");
              }else if(angular.equals(response.code,"success")){
                Utility.showAlert("Success","Added Successfully.");
              }else{
                 Utility.showAlert("Error","Failed to Add to Wish List,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
               Utility.showAlert("Error","Failed to Add to Wish List,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

   //Add to Cart
  $scope.addToCart = function() {
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("add_to_cart",customer_id,$scope.id,$scope.quantity.value).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Add in User Cart,Try after sometime.");
              }else if(angular.equals(response.code,"success")){
                UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                       $rootScope.cartcount = response.cart;
                 }, function(err) {
                });
                Utility.showAlert("Success","Added Successfully.");
              }else{
                 Utility.showAlert("Error","Error to Add in Cart,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
               Utility.showAlert("Error","Failed to Add in Cart,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  //show zoom view of images
  $scope.showImages = function() {
   $scope.showModal('templates/image-zoomview.html');
  }
  
 //Method to open template in modal
  $scope.showModal = function(templateUrl) {
	  $ionicModal.fromTemplateUrl(templateUrl, {
	    scope: $scope
	  }).then(function(modal) {
	    $scope.modal = modal;
	    $scope.modal.show();
	  });
   }
 
 //Method to close Modal
 $scope.closeModal = function() {
   $scope.modal.hide();
   $scope.modal.remove()
 };

 var zoomed=true;
 
 //Function to zoom image on tap
 $scope.zoomFunction = function() {
	if(zoomed){
		$ionicScrollDelegate.zoomBy(2, true);
	        zoomed = !zoomed;
	}else{
		$ionicScrollDelegate.zoomTo(1, true);
	        zoomed = !zoomed;
	}	
 };
})

 //Controller for Order Details 
.controller('OrderDetailsCtrl', function($scope,$rootScope, $stateParams,$ionicLoading,$ionicModal,
                                         $state,$ionicHistory,UserOrderInfo,Utility) {

   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.orderhistorys = [];
 
   //Show All History {"order_id":"","cnt":"","name":"Pending","firstname":"","lastname":"","total":"","date_added":""}
  $scope.showAllHistory =function(){
    $scope.orderhistorys = [];
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserOrderInfo.orderInformation("order_history",customer_id,0).then(function(response) {
              $ionicLoading.hide();
              $scope.orderhistorys = response.order_history;
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Show History,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

 //Show Order History Details
    $scope.showOrderDetails =function(product_id){
        $state.go('app.orderinfo',{'productId':product_id});
   }

//continue to shopping
  $scope.continueShoping =function(){
    $ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
               disableBack: true,
               historyRoot: true
    });
    $state.go('app.products');
  }

})

.controller('OrderInfoCtrl', function($scope,$rootScope,$stateParams,$ionicLoading,$state,$ionicModal,
                                      $ionicHistory,UserOrderInfo,UserOrderReturn,Utility) {

  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");
  var product_id = $stateParams.productId;

  $scope.user = {
                   firstname:"",
                   lastname:"",
                   email:"",
                   telephone:"",
                   order_id:"",
                   name:"", 
                   model:"",
                   quantity:"",
                   reason_for_return:"",
                   product_open:"",
                   product_id:"",
                   date_added:""
                   
                };

  $scope.reason = {
                   value1:"Dead On Arrival",
                   value2:"Received Wrong Item",
                   value3:"Order Error",
                   value4:"Faulty, please supply details",
                   value5:"Other, please supply details"
           };
  $scope.product_open = {
                   value1:"true",
                   value2:"false"
           };

  //Method to Order Info
  $scope.orderInfo =function(){
      $scope.oc_order_total=[];
      $scope.oc_order_product=[];
      if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserOrderInfo.orderInformation("order_details_information",customer_id,product_id).then(function(response) {
              $ionicLoading.hide();
              $scope.orderdetails = response.order_details[0];
              $scope.oc_order_total = response.oc_order_total;
              $scope.oc_order_product = response.oc_order_product;
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Show History,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   };
 
  //Show Order return Details
    $scope.orderReturn =function(order_id){
      if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserOrderReturn.orderReturnProduct("return_product_show_edit","",customer_id,order_id).then(function(response) {
              $ionicLoading.hide();
              $scope.user = response[0];
              //$scope.user.order_id= parseInt(response[0].order_id, 10);
              $scope.showModal('templates/order/order_return_edit.html');
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Show History,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   };
  
 //Show Order return submits
    $scope.orderReturnSubmit =function(){
      if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserOrderReturn.orderReturnProduct("return_product_add",$scope.user,customer_id,0).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                 $scope.closeModal();
                 $ionicHistory.clearHistory();
                 $ionicHistory.nextViewOptions({
                               disableBack: true,
                               historyRoot: true
                  });
                 $state.go('app.products');
                 Utility.showAlert("Success","Successfully Return Product.");
              }else{
                   Utility.showAlert("Error","Failed to Return Product,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Return Product,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   };

  //Method to open template in modal
  $scope.showModal = function(templateUrl) {
	  $ionicModal.fromTemplateUrl(templateUrl, {
	    scope: $scope
	  }).then(function(modal) {
	    $scope.modal = modal;
	    $scope.modal.show();
	  });
   };

  //Method to close modal
   $scope.closeModal = function() {
     $scope.modal.hide();
     $scope.modal.remove()
 };

})

 //Controller for Order Returns  
.controller('OrderReturnsCtrl', function($scope,$rootScope, $stateParams ,$state,$ionicHistory,$ionicLoading,UserOrderReturn,Utility) {

  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");
  $scope.orderreturnhistorys = [];
 
  $scope.showAllReturnHistory =function(){
    $scope.orderreturnhistorys = [];
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserOrderReturn.orderReturnProduct("return_show_product","",customer_id,0).then(function(response) {
              $ionicLoading.hide();
              $scope.orderreturnhistorys = response.return_show_product;
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Show History,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

 //Show Order History Details
    $scope.showReturnDetails =function(return_id){
        $state.go('app.orderreturndetail',{'returnId':return_id});
   };

//continue to shopping
  $scope.continueShoping =function(){
    $ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
               disableBack: true,
               historyRoot: true
    });
    $state.go('app.products');
  };

})

//order return Details Controller
.controller('OrderReturnsDetailsCtrl', function($scope,$rootScope, $stateParams ,$state,$ionicHistory,$ionicLoading,UserOrderReturn,Utility) {
  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");
  var return_id = $stateParams.returnId;
/*
    "return_id":"","order_id":"","product":"","model":"","quantity":"","date_ordered":"","date_added":"","name":"Order Error","opened":""
*/

 //Method to Order Info
  $scope.orderReturnInfo =function(){
      if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserOrderReturn.orderReturnProduct("return_product_information","",0,return_id).then(function(response) {
              $ionicLoading.hide();
              $scope.return_product = response.return_product[0];
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Show History,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   };
})

//Controller for User User Cart  
.controller('UserCartCtrl', function($scope,$rootScope,$state,$ionicModal,$stateParams,$ionicLoading,
                                     $ionicHistory,UserCart,WishList,Utility) { 

   $scope.BASE_URL = BASE_URL; 
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.cartlists = [];
   $scope.total=0;
   
   //continue to shopping
  $scope.continueShoping =function(){
    $ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
               disableBack: true,
               historyRoot: true
    });
    $state.go('app.products');
  };

  //Show All Cart
  $scope.showAllCart =function(product){
    $scope.total=0;
    if($rootScope.loginstatus == true){
          UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                            $rootScope.cartcount = response.cart;
                    }, function(err) {
            });
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("cart_show_all",customer_id,0,0).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                 var singlecart = 0;
                 $scope.cartlists = response.cart_show_all;
                 response.cart_show_all.forEach(function(element, index, array){
		          singlecart=element.price*element.quantity;
                          $scope.total += singlecart;
	         });
                //"model":"","price":"","image":"","name":"","product_id":"","quantity":}
              }else{
                  $scope.cartlists = [];
              } 
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Show Cart,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  //Update  Cart
  $scope.updateCart =function(product){
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("cart_update",customer_id,product.product_id,product.quantity).then(function(response) {
               $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Update Cart,Try after sometime.");
              }else if(angular.equals(response.code,"success")){
                $scope.showAllCart();
                Utility.showAlert("Success","Successfully Update.");
              }else{
                 Utility.showAlert("Error","Error to Add in Cart,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Update Cart,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  //Remove Product From Cart List
  $scope.removeProductFromCart =function(product){
   if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("cart_remove",customer_id,product.product_id).then(function(response) {
              $ionicLoading.hide();
             if(angular.equals(response.code,"success")){
                  var index = $scope.cartlists.indexOf(product);
                  $scope.cartlists.splice(index, 1);   
                   UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                            $rootScope.cartcount = response.cart;
                    }, function(err) {
                   });
                  Utility.showToast("Successfully remove from Cartlist");
              }else{
                Utility.showAlert("Error","Error to remove ,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to remove ,Try after sometime.");
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
     }
  };

  //Add to wish list
  $scope.addToWishList = function(product) {
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           WishList.userWishlist("wishlist_add_product",customer_id,product.product_id).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Add to Wish List,Try after sometime.");
              }else if(angular.equals(response.code,"exist")){
                 Utility.showAlert("Success","Allready Exist in Wishlist");
              }else if(angular.equals(response.code,"success")){
                Utility.showAlert("Success","Added Successfully.");
              }else{
                 Utility.showAlert("Error","Failed to Add to Wish List,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Add to Wish List,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  //user checkout
   $scope.userCheckout = function() {
    $state.go('app.billcheckout');
  }

   $scope.showModal = function(templateUrl) {
	  $ionicModal.fromTemplateUrl(templateUrl, {
	    scope: $scope
	  }).then(function(modal) {
	    $scope.modal = modal;
	    $scope.modal.show();
	  });
   };
 
  $scope.closeModal = function() {
    $scope.modal.hide();
    $scope.modal.remove()
  };
})

//Controller to Billing Address for checkout
.controller('BillCheckoutCtrl', function($scope,$rootScope, $state,$stateParams,$ionicLoading,
                                         $ionicModal,Utility,UserCart,CheckOut,AddressInfo) {

   $scope.BASE_URL = BASE_URL;
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.billexist={address:"A"};
   $scope.bill={value1:"A",value2:"B"};
   $scope.validstatus = false;
   $scope.user={
                 bill_firstname:"",bill_lastname:"",bill_address_1:"",bill_address_2:"",bill_city:"",
                 bill_postcode:"",bill_country_name:"",bill_zone_name:""
              };

   if($rootScope.loginstatus == true){
       
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("customer_account_address_get_show",customer_id,0).then(function(response) {
                $scope.checkoutaddress = response.customer_account_address_get_show[0];
                $ionicLoading.hide();
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to get Address ,Try after sometime.");
         });
     }else{
          Utility.showAlert("Error","Please Login First..");
          
     }
 
//Method to passed parameter to next state
 $scope.billCheckout = function() {
   $state.go('app.deliverycheckout', 
                          {
                             'isExist': $scope.billexist.address,
                             'firstname':$scope.user.bill_firstname,
                             'lastname':$scope.user.bill_lastname,
                             'address_1':$scope.user.bill_address_1,
                             'address_2':$scope.user.bill_address_2,
                             'city':$scope.user.bill_city,
                             'postcode':$scope.user.bill_postcode,
                             'country_name':$scope.user.bill_country_name,
                             'zone_name':$scope.user.bill_zone_name
                            });
  };
  
  
  
})

//Controller to Delivery Address for checkout
.controller('DeliveryCheckoutCtrl', function($scope,$rootScope,$state,$stateParams,$ionicLoading,
                                             $ionicModal,UserCart,CheckOut,Utility) {


   $scope.BASE_URL = BASE_URL;  
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.deliveryexist={address:"A"};
   $scope.bill={value1:"A",value2:"B"};
   $scope.tax=0;
   $scope.billexist={address:$stateParams.isExist};

   $scope.user={
                 bill_firstname:$stateParams.firstname,
                 bill_lastname:$stateParams.lastname,
                 bill_address_1:$stateParams.address_1,
                 bill_address_2:$stateParams.address_2,
                 bill_city:$stateParams.city,
                 bill_postcode:$stateParams.postcode,
                 bill_country_name:$stateParams.country_name,
                 bill_zone_name:$stateParams.zone_name,
                 del_firstname:$stateParams.firstname,
                 del_lastname:$stateParams.lastname,
                 del_address_1:$stateParams.address_1,
                 del_address_2:$stateParams.address_2,
                 del_city:$stateParams.city,
                 del_postcode:$stateParams.postcode,
                 del_country_name:$stateParams.country_name,
                 del_zone_name:$stateParams.zone_name
              };
  
  if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("customer_account_address_get_show",customer_id,0).then(function(response) {
                $scope.checkoutaddress = response.customer_account_address_get_show[0];
                if($scope.checkoutaddress.tax>0){
                    $scope.tax = $scope.checkoutaddress.tax
                }
                $ionicLoading.hide();
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to get Address ,Try after sometime.");
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
     }
 
//Method to save address to server
 $scope.continueCheckout = function() {
      if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });

           if(angular.equals($scope.billexist.address,"A") && angular.equals($scope.deliveryexist.address,"A")){
              CheckOut.userCheckOut("order_add_bill_exist_delivery_exist",customer_id,0,0).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                $state.go('app.deliverymethodcheckout',{"checkoutTax":$scope.checkoutaddress.tax});
              }else{
                   Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
              } 
           }, function(err) {
               $ionicLoading.hide();
               Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
          });
        }else if(angular.equals($scope.billexist.address,"B") && angular.equals($scope.deliveryexist.address,"A")){
            CheckOut.userCheckOut("order_add_bill_new_delivery_exist",customer_id,0,$scope.user).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                $state.go('app.deliverymethodcheckout',{"checkoutTax":$scope.checkoutaddress.tax});
              }else{
                   Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
              } 
           }, function(err) {
               $ionicLoading.hide();
               Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
          });
        }else if(angular.equals($scope.billexist.address,"A") && angular.equals($scope.deliveryexist.address,"B")){
            CheckOut.userCheckOut("order_add_bill_exist_delivery_new",customer_id,0,$scope.user).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                $state.go('app.deliverymethodcheckout',{"checkoutTax":$scope.checkoutaddress.tax});
              }else{
                  Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
              } 
           }, function(err) {
               $ionicLoading.hide();
               Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
          });
        }else if(angular.equals($scope.billexist.address,"B") && angular.equals($scope.deliveryexist.address,"B")){
           CheckOut.userCheckOut("order_add_bill_new_delivery_new",customer_id,0,$scope.user).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                $state.go('app.deliverymethodcheckout',{"checkoutTax":$scope.checkoutaddress.tax});
              }else{
                   Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
              } 
           }, function(err) {
               $ionicLoading.hide();
               Utility.showAlert("Error","Failed to Save Address,Try after sometime.");
          });
        }
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };


//Method to save address
 $scope.deliveryCheckout = function() {
   $scope.continueCheckout();
  };
})

//Controller to delivery method checkout
.controller('DeliveryMethodCheckoutCtrl', function($scope, $state,$stateParams) {

   $scope.BASE_URL = BASE_URL; 
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.delivery={method: "",comment:""};
   $scope.deliverymethod={value: ""};
   $scope.tax=$stateParams.checkoutTax;

  //Go to Show payment method
  $scope.goToPayment = function() {
   $state.go('app.paymentmethodcheckout',{"checkoutTax":$scope.tax});
  };

})


//Controller to payment method checkout
.controller('PaymentMethodCheckoutCtrl', function($scope,$rootScope, $state,$stateParams,$ionicLoading,
                                                  $ionicModal,$ionicHistory,UserCart,CustomizeOrder,Utility) {

   $scope.BASE_URL = BASE_URL; 
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.payment={method: true,agreement:false,comment:""};
   $scope.paymentmethod={value: true};

   $scope.partialpayment={flag:false,value1:true,value2:false};
   
   if($rootScope.userwholesaler == true)
      $scope.partialpaymentpercentage = wholesaler_partial_payment;
   else
      $scope.partialpaymentpercentage = normal_partial_payment;

    var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });

   CustomizeOrder.userCustomizeOrder("customized_order_list",customer_id,"","").then(function(response) {
               $ionicLoading.hide();
               $scope.customizeorderlist =  response.custom_order_list;
               if($scope.customizeorderlist != undefined && $scope.customizeorderlist.length > 0){
                 $rootScope.customize_order_status = true ;
               }else{
                    $rootScope.customize_order_status = false ;
               }
           }, function(err) {
               $ionicLoading.hide();
   });

   $scope.tax=$stateParams.checkoutTax;

    //Go to confirm order
  $scope.goToConfirm = function() {
    if($scope.payment.agreement == true){
       $state.go('app.showorderproduct',{"checkoutTax":$scope.tax,"partialPayment":$scope.partialpayment.flag});
    }else{
       Utility.showAlert("Check","Please Check the Terms & Conditions.");
    }
  };
})


//Controller to show Products of confirm order
.controller('ShowOrderProductCtrl', function($scope,$rootScope, $state,$stateParams,$ionicLoading,$ionicModal,
                                             $ionicHistory,UserCart,CheckOut,Utility,CustomizeOrder) {
   var flag = false;
   $scope.BASE_URL = BASE_URL; 
   var email = window.localStorage.getItem("useremail");
   var customer_id = window.localStorage.getItem("customer_id");
   $scope.tax=$stateParams.checkoutTax;
   $scope.partialpayment=$stateParams.partialPayment;

   $scope.cartlists = [];
   $scope.total=0;
   $scope.VAT=(($scope.tax*14)/100).toFixed(2);
   $scope.grandTotal=0;

   if($rootScope.userwholesaler == true)
      $scope.partialpaymentpercentage = wholesaler_partial_payment;
   else
      $scope.partialpaymentpercentage = normal_partial_payment;

  //Show All Cart
  $scope.showAllCart =function(product){
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           if(angular.equals($scope.partialpayment,"true")){
                    CustomizeOrder.userCustomizeOrder("customized_order_list",customer_id,"","").then(function(response) {
		           $scope.customizeorderlist =  response.custom_order_list;
                           if($scope.customizeorderlist != undefined && $scope.customizeorderlist.length > 0){
                                 $rootScope.customize_order_status = true ;
                          }else{
                                 $rootScope.customize_order_status = false ;
                          }
                      }, function(err) {
                     });

		     UserCart.userCart("cart_show_all",customer_id,0,0).then(function(response) {
		      $ionicLoading.hide();
		      if(angular.equals(response.code,"success")){
		         var singlecart = 0;
		         $scope.cartlists = response.cart_show_all;
		         response.cart_show_all.forEach(function(element, index, array){
                                if($scope.customizeorderlist !=undefined && $scope.customizeorderlist.length>0){
                                       $scope.customizeorderlist.forEach(function(customizelement, customizindex, array){
                                            if(customizelement.product_id == element.product_id){
                                               flag = true;
                                            }else{
                                                flag = false;
                                            }
                                              singlecart=element.price*element.quantity;
		                              $scope.total += singlecart;
			               });
                                  if(flag == true){
                                     $scope.total = ($scope.total*$scope.partialpaymentpercentage)/100;
                                     $scope.cartlists[index].customizetoal =  $scope.total ;
                                     $scope.cartlists[index].flag =  true; 
                                  }else{
                                       $scope.cartlists[index].flag =  false; 
                                  }
                                }else{
                                        singlecart=element.price*element.quantity;
		                        $scope.total += singlecart;
                                }
				 
			 });
		          $scope.grandTotal=parseFloat($scope.total)+parseFloat($scope.tax)+parseFloat($scope.VAT);
		        //"model":"","price":"","image":"","name":"","product_id":"","quantity":}
		      }else{
		          $scope.cartlists = [];
		      } 
		    }, function(err) {
		       $ionicLoading.hide();
		       Utility.showAlert("Error","Failed to Show Cart,Try after sometime.");
		   });
           }else{
		   UserCart.userCart("cart_show_all",customer_id,0,0).then(function(response) {
		      $ionicLoading.hide();
		      if(angular.equals(response.code,"success")){
		         var singlecart = 0;
		         $scope.cartlists = response.cart_show_all;
		         response.cart_show_all.forEach(function(element, index, array){
				  singlecart=element.price*element.quantity;
		                  $scope.total += singlecart;
			 });
		          $scope.grandTotal=parseFloat($scope.total)+parseFloat($scope.tax)+parseFloat($scope.VAT);
		        //"model":"","price":"","image":"","name":"","product_id":"","quantity":}
		      }else{
		          $scope.cartlists = [];
		      } 
		   }, function(err) {
		      $ionicLoading.hide();
		      Utility.showAlert("Error","Failed to Show Cart,Try after sometime.");
		 });
       }
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };
  
 
 $scope.userorder= {
                       "subtotal":$scope.total,
                       "shippingrate":$scope.tax,
                       "vat":$scope.VAT,
                       "grandtotal":$scope.grandTotal
                     };

 //Confirm Order
   $scope.confirmOrder =function(){
	    if($rootScope.loginstatus == true){
		   var loading = $ionicLoading.show({
	  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
		   });
		   CheckOut.userCheckOut("checkout_add",customer_id,0, $scope.userorder).then(function(response) {
		      $ionicLoading.hide();
                      UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                       $rootScope.cartcount = response.cart;
                       }, function(err) {
                      });
		      if(angular.equals(response.code,"success")){
                         $ionicHistory.clearHistory()
                         $ionicHistory.nextViewOptions({
                               disableBack: true,
                               historyRoot: true
                         });
                         $state.go('app.products');
                         Utility.showAlert("Success","Successfully Place Your Order");
		      }else{
		        Utility.showAlert("Error","Failed to Confirm Order,Try after sometime.");
		      } 
		   }, function(err) {
		      $ionicLoading.hide();
		      Utility.showAlert("Error","Failed to Confirm Order,Try after sometime.");
		 });
	     }else{
		 Utility.showAlert("Error","Please Login First..");
	     }
  };
})

 //Controller for User Account  
.controller('UserAccountCtrl', function($scope,$rootScope,$state, $ionicHistory,$ionicModal,
                                        $stateParams, $ionicLoading,UserInfo,WishList,UserCart,Utility) {

     var email = window.localStorage.getItem("useremail");
     var customer_id = window.localStorage.getItem("customer_id");
     var imei_number = "";
     $scope.BASE_URL = BASE_URL;
     $scope.quantity = { value:  1 };
      $scope.user = {
                   firstname:"",
                   lastname:"",
                   email:"",
                   telephone:"",
                   fax:"",
                   customer_id:"",
                   company:"",
                   address_1:"",
                   address_2:"",
                   city:"",
                   postcode:"",
                   country_name:"",
                   zone_name:""
                };
     $scope.wishlists = [];
      
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserInfo.userInformation("","customer_account_info_get",customer_id,imei_number).then(function(response) {
              $ionicLoading.hide();
              $scope.user = response[0];
           }, function(err) {
              $ionicLoading.hide();
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
     }

 //continue to shopping
  $scope.continueShoping =function(){
    $ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
               disableBack: true,
               historyRoot: true
    });
    $state.go('app.products');
  };
  
 //Get User Address Information
  $scope.getUserAddressInformation = function() {
     $scope.address = null;
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserInfo.userInformation("","customer_account_address_get",customer_id,imei_number).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"failed")){
                Utility.showAlert("Error","Failed to Get User Address Information.");
              }else if(angular.equals(response.code,"error")){
                Utility.showAlert("Error","Error to Get User Address Information.");
              }else{
                $scope.address = response[0];
              }
           }, function(err) {
              $ionicLoading.hide();
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };


  //updateUserInformation

  $scope.updateUserInformation = function() {
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserInfo.userInformation($scope.user,"customer_account_info_update",customer_id,imei_number).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"failed")){
                 Utility.showAlert("Error","Failed to Update User Information.");
              }else if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Error to Update User Information.");
              }else{
                $scope.closeModal();
                Utility.showAlert("Success","User Information Updated Successfully.");
                window.localStorage.setItem("useremail", email);
              }
           }, function(err) {
              $ionicLoading.hide();
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  //update user password
   $scope.updateUserPassword = function() {
    if(angular.equals($scope.user.newpassword,$scope.user.confirmePassword)){
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserInfo.userInformation($scope.user,"customer_account_password_update",customer_id,imei_number).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"failed")){
                 Utility.showAlert("Error","Failed to Update User Password.");
              }else if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Error to Update User Password.");
              }else{
                $scope.closeModal();
                Utility.showAlert("Success","User Password Updated Successfully.");
              }
           }, function(err) {
              $ionicLoading.hide();
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   }else{
         Utility.showAlert("Error","Please Enter New and Confirme Password Same.");
   }
  };
   
   //updateUserInformation

  $scope.updateAddressInformation = function() {
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserInfo.userInformation($scope.address,"customer_account_address_update",customer_id,imei_number).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"failed")){
                 Utility.showAlert("Error","Failed to Update User Address Information.");
              }else if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Please Fill Correct Country or State.");
              }else{
                $scope.closeModal();
                Utility.showAlert("Success","User Address Information Updated Successfully.");
              }
           }, function(err) {
              $ionicLoading.hide();
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  //Show user wish list
  $scope.showUserWishList =function(){
   if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           WishList.userWishlist("wishlist_show_all",customer_id,0).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                 $scope.wishlists = response.wishlist_show_all;
                 if($scope.wishlists.lenght>0 && $scope.wishlists != undefined){
                     $scope.showModal('templates/user/user-wish-list.html');
                 }else{
                   Utility.showToast("Wishlist is Empty !");
                 }
              }else{
                  $scope.wishlists = [];
              } 
              //{"model":"","price":"","image":"","name":"","product_id":""}
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to Show WishList ,Try after sometime.");
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
     }
  };
  
  //Remove Product From Wish List
  $scope.removeProductFromWishList =function(product){
   if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           WishList.userWishlist("wishlist_product_remove",customer_id,product.product_id).then(function(response) {
              $ionicLoading.hide();
             if(angular.equals(response.code,"success")){
                  var index = $scope.wishlists.indexOf(product);
                  $scope.wishlists.splice(index, 1);     
                  Utility.showToast("Successfully remove from WishList");
              }else{
               Utility.showAlert("Error","Error to remove ,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to remove ,Try after sometime.");
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
     }
  };
 
    //Add to Cart
  $scope.addToCart = function(product) {
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("add_to_cart",customer_id,product.product_id,$scope.quantity.value).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Add in User Cart,Try after sometime.");
              }else if(angular.equals(response.code,"success")){
               UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                       $rootScope.cartcount = response.cart;
                 }, function(err) {
                });
                Utility.showAlert("Success","Added Successfully.");
              }else{
                 Utility.showAlert("Error","Error to Add in Cart,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Add in Cart,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };

  /**
    User Update Information Modal Views & Methods
   **/
  $scope.userInformation = function() {
   $scope.showModal('templates/user/user-personal-details.html');
  }
  
  $scope.changePassword = function() {
   $scope.showModal('templates/user/change-password.html');
  }

 $scope.changeAddress= function() {
   $scope.getUserAddressInformation();
   $scope.showModal('templates/user/user-address-details.html');
  }
  
  $scope.userWishList= function() {
   $scope.showUserWishList();
  }
  
  $scope.userOrderHistory= function() {
      $state.go('app.orderdetails');
  }
  
  $scope.customizeOrder= function() {
       $state.go('app.customizeOrder');
  }	

  $scope.customizeOrderList= function() {
      $state.go('app.customizeOrderList');
  }	

  $scope.userReturnRequests= function() {
    $state.go('app.orderreturns');
  }
  
  $scope.userTransactions= function() {
   //$scope.showModal('templates/user/user-wish-list.html');
  }
 
  $scope.showModal = function(templateUrl) {
	  $ionicModal.fromTemplateUrl(templateUrl, {
	    scope: $scope
	  }).then(function(modal) {
	    $scope.modal = modal;
	    $scope.modal.show();
	  });
   }
 
 $scope.closeModal = function() {
   $scope.modal.hide();
   $scope.modal.remove()
 };
})

 //Controller for Wishlist  
.controller('WishlistCtrl', function($scope,$rootScope, $stateParams, $state,$ionicHistory,
                                     $ionicLoading,WishList,UserCart,Utility) {

  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");
  var imei_number = "";
  $scope.BASE_URL = BASE_URL;
  $scope.quantity = { value:  1 };
  $scope.wishlists = [];
  //Show user wish list
   if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           WishList.userWishlist("wishlist_show_all",customer_id,0).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"success")){
                 $scope.wishlists = response.wishlist_show_all;
              }else{
                  $scope.wishlists = [];
              } 
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to Show WishList ,Try after sometime.");
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
    }
  
  //continue to shopping
   $scope.continueShoping =function(){
    $ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
               disableBack: true,
               historyRoot: true
    });
    $state.go('app.products');
  };

  //Add to Cart
  $scope.addToCart = function(product) {
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("add_to_cart",customer_id,product.product_id,$scope.quantity.value).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Add in User Cart,Try after sometime.");
              }else if(angular.equals(response.code,"success")){
                //
               UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                       $rootScope.cartcount = response.cart;
                 }, function(err) {
                });
                Utility.showAlert("Success","Added Successfully.");
              }else{
                Utility.showAlert("Error","Error to Add in Cart,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Add in Cart,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };
  
  //Remove Product From Wish List
  $scope.removeProductFromWishList =function(product){
   if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           WishList.userWishlist("wishlist_product_remove",customer_id,product.product_id).then(function(response) {
              $ionicLoading.hide();
             if(angular.equals(response.code,"success")){
                  var index = $scope.wishlists.indexOf(product);
                  $scope.wishlists.splice(index, 1);     
                  Utility.showToast("Successfully remove from WishList");
              }else{
                Utility.showAlert("Error","Error to remove ,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Error to remove ,Try after sometime.");
         });
     }else{
           Utility.showAlert("Error","Please Login First..");
     }
  };
})

 //Controller for Wholeseller  
.controller('WholesalerCtrl', function($scope,$rootScope, $stateParams,$ionicLoading,$rootScope,UserCart,Wholesaler,Utility) {

  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");  

  $scope.BASE_URL = BASE_URL;
  $scope.product = { quantity:  1 };

  //show wholeseller products
  $scope.showProduct = function(){
    $scope.products = [] ;
    //$scope.products = Wholesaler.getStaticWhollseller();
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           /*
                 wholesale: {product_id,name,model,image,minimum_qty_for_wholesaler,price,size:{product_id,quantity,price,name}}
           */
           Wholesaler.getWhollseller("wholesaler").then(function(response) {
              $ionicLoading.hide();
              $scope.products = response.wholesale;
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Load,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };
  
   //Add to Cart
  $scope.addToCart = function(product) {

  if(product.quantity>=product.minimum_qty_for_wholesaler){
    if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           UserCart.userCart("add_to_cart",customer_id,product.product_id,product.quantity).then(function(response) {
              $ionicLoading.hide();
              if(angular.equals(response.code,"error")){
                 Utility.showAlert("Error","Failed to Add in User Cart,Try after sometime.");
              }else if(angular.equals(response.code,"success")){
               UserCart.userCart("count_cart_wishlist",customer_id,0,0).then(function(response) {
                       $rootScope.cartcount = response.cart;
                 }, function(err) {
                });
                Utility.showAlert("Success","Added Successfully.");
              }else{
                 Utility.showAlert("Error","Error to Add in Cart,Try after sometime.");
              }
           }, function(err) {
              $ionicLoading.hide();
              Utility.showAlert("Error","Failed to Add in Cart,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   }else{
     Utility.showAlert("Error","Minimun Quantity Should be : "+product.minimum_qty_for_wholesaler);
   }
  };
  
})


//Controller for Customize Order insert 
.controller('CustomizeOrderCtrl', function($scope,$rootScope, $stateParams,$ionicPlatform,$cordovaCamera,$ionicLoading,$filter,
                                           $state, $ionicHistory,CustomizeOrder,Utility) {

  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");  
	
        $scope.user = {
                          productname: "",
                          productsize : "",
                          productquantity: "",
                          productdecription: "",
                          comment:"",
                          expected_date : new Date(),
                          image_title: "",
                          min_date :  new Date()
                      };

	

       /*  $scope.$watch('user.expected_date', function (newValue) {
               $scope.user.expect_date  = $filter('date')(newValue, 'yyyy/MM/dd'); 
         });
        */
	$scope.takePhoto = function () {
          $ionicPlatform.ready(function() {
             if(ionic.Platform.isWebView()){
               var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 300,
                    targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
  
                $cordovaCamera.getPicture(options).then(function (imageData) {
                        $scope.imgURI = "data:image/jpeg;base64," + imageData;
                        //alert("Hi :"+JSON.stringify($scope.imgURI));
                }, function (err) {
                        // An error occured. Show a message to the user
                });
             }else{
               Utility.showAlert("Error","Camera Not Support..");
             }
          });
        };

         $scope.choosePhoto = function () {
              $ionicPlatform.ready(function() {
               if(ionic.Platform.isWebView()){
                var options = {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 300,
                    targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false
                };
  
                $cordovaCamera.getPicture(options).then(function (imageData) {
                        $scope.imgURI = "data:image/jpeg;base64," + imageData;
                }, function (err) {
                        // An error occured. Show a message to the user
                });
               }else{
               Utility.showAlert("Error","Camera Not Support..");
              }
            });
        };
        
       //Post Customize Order
       $scope.uploadCustomizedOrder = function(userorder) {
         userorder.expected_date  = $filter('date')(userorder.expected_date, 'yyyy-MM-dd');
        // alert(JSON.stringify(userorder));
         if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           CustomizeOrder.userCustomizeOrder("customized_order",customer_id,userorder,$scope.imgURI).then(function(response) {
               $ionicLoading.hide();
               if(angular.equals(response.code,"success")){
                  $ionicHistory.clearHistory()
                  $ionicHistory.nextViewOptions({
                     disableBack: true,
                     historyRoot: true
                  });
                 $state.go('app.products');
                 Utility.showAlert("Success","Successfully submit Order.");
               }else{
                Utility.showAlert("Error","Failed to submit Order");
               }
           }, function(err) {
               $ionicLoading.hide();
              Utility.showAlert("Error","Failed to submit Order,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
  };
})

//CustomizeOrderList
.controller('CustomizeOrderListCtrl', function($scope,$rootScope, $stateParams,$ionicLoading,$ionicHistory,$state,CustomizeOrder,Utility) {
  var email = window.localStorage.getItem("useremail");
  var customer_id = window.localStorage.getItem("customer_id");  
  $scope.BASE_URL = BASE_URL;
  $scope.showCustomizeList = function() {
     if($rootScope.loginstatus == true){
           var loading = $ionicLoading.show({
  			  template: '<ion-spinner icon="bubbles" class="spinner-balanced"></ion-spinner>'
           });
           CustomizeOrder.userCustomizeOrder("customized_order_list",customer_id,"","").then(function(response) {
               $ionicLoading.hide();
               $scope.customizeorderlist =  response.custom_order_list;
           }, function(err) {
               $ionicLoading.hide();
               //alert(JSON.stringify(err));
               Utility.showAlert("Error","Failed to get List,Try after sometime.");
         });
     }else{
         Utility.showAlert("Error","Please Login First..");
     }
   };
  
  //continue to shopping
  $scope.continueShoping =function(){
    $ionicHistory.clearHistory()
    $ionicHistory.nextViewOptions({
               disableBack: true,
               historyRoot: true
    });
    $state.go('app.products');
  };
})

 //Controller for Contact Us Details 
.controller('ContactUsCtrl', function($scope, $stateParams) {
   $scope.BASE_URL = BASE_URL;
   $scope.name = "Bbj Export";
   $scope.image = "img/logo.png";
   $scope.callnumber = " 08048112701";
   $scope.address = "Baldev (Proprietor) No. 1702/03, RS Kedari Road, Pune Camp Pune - 411001, Maharashtra, India ";
})

 //Controller for Help  
.controller('HelpCtrl', function($scope, $stateParams) {
})


 //Controller for About Us Details  
.controller('AboutUsCtrl', function($scope, $stateParams) {
});